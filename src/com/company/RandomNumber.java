package com.company;

import java.util.Random;
import java.util.Scanner;

public class RandomNumber {
    Random random = new Random();
    Scanner scanner = new Scanner(System.in);

    public RandomNumber() {
        getSingleNum();
        getTenNumber();
        getMaxTenNumber();
        getMinMaxNumber();
        getMinusMaxNumber();
        getRandomQuantityNum();
    }

    private void getSingleNum() {
        System.out.println("Output in console random number.");
        int getNumber = random.nextInt();
        System.out.println("You single random number: " + getNumber);
    }

    private void getTenNumber() {
        System.out.println("Output in console 10 random number.");
        for (int i = 1; i <= 10; i++) {
            int getNumber = random.nextInt();
            System.out.printf("You %d random number: %d %n", i, getNumber);
        }
    }

    private void getMaxTenNumber() {
        System.out.println("Output in console 10 number min0-10max.");
        for (int i = 1; i <= 10; i++) {
            int getNumber = random.nextInt(10);
            System.out.printf("You %d random number: %d %n", i, getNumber);
        }
    }

    private void getMinMaxNumber() {
        System.out.println("Output in console 10 number min20-50max.");
        for (int i = 1; i <= 10; i++) {
            int getNumber = random.nextInt(50);
            if (getNumber >= 20) {
                System.out.printf("You %d random number: %d %n", i, getNumber);
            } else {
                i--;
            }
        }
    }

    private void getMinusMaxNumber() {
        System.out.println("Output in console 10 number min-10-10max.");
        for (int i = 1; i <= 10; i++) {
            int getNumber = random.nextInt(10);
            System.out.printf("You %d random number: %d %n", i, getNumber);
            getNumber -= 10;
            System.out.printf("You %d random number: %d %n", ++i, getNumber);
        }
    }

    private void getRandomQuantityNum() {
        System.out.println("Output in console random quantity number min-10-35max.");
        int setRandomQuantity = random.nextInt(15);
        if (setRandomQuantity < 3) {
            setRandomQuantity += 10;
        }
        for (int i = setRandomQuantity; i > 0; i--) {
            int getNumber = random.nextInt(35);
            if (getNumber <= 10) {
                getNumber -= 10;
            }
            System.out.println(getNumber);
        }
    }
}

